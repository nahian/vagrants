#!/usr/bin/env bash

# add required packages
apt-get update
apt-get install -y python-software-properties
apt-add-repository -y ppa:rquillo/ansible
apt-get update
apt-get install -y ansible sshpass git

# Create inventory file for ansible
echo "localhost ansible_connection=local

[kitchen]
localhost ansible_connection=local

[ecehost]
# <your_ece_host> ansible_connection=ssh ansible_ssh_user=<user> ansible_ssh_pass=<pass>
# you may want to use ssh key based login instead of user pass" > /etc/ansible/hosts


work=/vagrant/work

# download builder playbook
mkdir -p $work
cd $work
git clone https://nahian@bitbucket.org/nahian/ansibles.git
cd $work/ansibles
git pull -u origin master

# run builder setup playbook
cd $work/ansibles/ece
 su -c "ansible-playbook setup-kitchen.yml --skip-tags=aptupdate" vagrant